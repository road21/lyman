# Plans

- [x] **Setup**: \
  minimal Intermediate Representation \
  POC: translation to dotty trees, running via reflection.
  *Example*: expression `(1 + 2 + 7) / (2 * 1)` (without parsing) translated to  
  ```scala
  @main def main(): Unit = println((1 + 2 + 7) / (2 * 1))
  ```
  (on tree level).
- [ ] **Input/Output types and values** \
  Translate AST to scala function with context and output type.
  Create default library (that will move to Data Metric rep) with methods:
  ```scala
    type MetricType = Boolean | String | BigDecimal // ...
    case class DMContext(metrics: Map[UUID, MetricType]) extends Context
    def metric[T <: MetricType](uuid: UUID)(using Context): T =
      summon[Context].asInstanceOf[DMContext].metrics(uuid).asInstanceOf[T]
  ```
  *Example*: expression (ast level) `metric[Double]("3d746730-f49e-47dc-8977-d1bcad82de4c") / metric[Double]("2c6d3c92-74b1-11ed-a1eb-0242ac120002")` \
  Output type: `Double` \
  Translated to:
  ```scala
  def run(ctx: Context): Double =
    given ctx = Context
    metric[Double]("3d746730-f49e-47dc-8977-d1bcad82de4c") / metric[Double]("2c6d3c92-74b1-11ed-a1eb-0242ac120002")
  ```
  Can be applied to value: `DMContext(Map("3d746730-f49e-47dc-8977-d1bcad82de4c" -> 0.1, "2c6d3c92-74b1-11ed-a1eb-0242ac120002" -> 0.5))` \
  and we can get the output result.
  Add `compileAndRun` method that works with AST.
- [ ] **Simple Parser** \
  Research applicative parsers, choose parser library. \
  Implement simple parser working only with one-line expressions. \
  Add `parse` method.
- [ ] **Bytecode machinery** \
  How to store? How to run? \
  Split `compileAndRun method` to `compile` and `run` methods.
- [ ] **Libraries** \
  Move library to DM repo. \
  Extend the API to be able to work with external libraries.\
  Extend library with `iff` and arithmetic methods
- [ ] **Error tracing** \
  Each compilation error should contain information about error location. \
  Probably, each node in IR ast should somehow contain it.

# Future plans

- [ ] **Extend parser and IR**
  We want to implement homoiconic expression lanaguge based on yaml syntax.
  Add to IR `let`, `defn`.
  ```yml
  let:
    x: log(metric1.x) + (metric2.y / metric3.z)
  in:
    iff(x > 50, RED, iff(x > 10, YELLOW, GREEN))
  ```