ThisBuild / version := "0.1.0"

ThisBuild / scalaVersion := "3.2.1"

libraryDependencies ++= Seq(
  "org.scala-lang" %% "scala3-compiler" % "3.2.1",
  "org.scala-lang" %% "scala3-library" % "3.2.1",
  "org.scalameta" %% "munit" % "0.7.29" % Test
)

val modules = file("modules")

lazy val langCore = project
  .in(modules / "lang-core")
  .settings(name := "stdlib")

lazy val utils = project
  .in(modules / "utils")
  .settings(name := "utils")
  .settings(libraryDependencies ++= Seq(
    "io.circe"       %% "circe-core"       % Version.circe,
    "io.circe"       %% "circe-generic"    % Version.circe
  ))

lazy val lang = project
  .in(modules / "lang")
  .settings(name := "lyman")
  .settings(
    libraryDependencies ++= Seq(
      "org.typelevel"  %% "cats-parse"       % Version.catsParse,
      "org.typelevel"  %% "kittens"          % Version.kittens,
      "org.scala-lang" %% "scala3-compiler"  % Version.scalaLib,
      "org.scala-lang" %% "scala3-library"   % Version.scalaLib,
      "io.circe"       %% "circe-core"       % Version.circe,
      "io.circe"       %% "circe-generic"    % Version.circe,
      "com.beachape"   %% "enumeratum"       % Version.enumeratum,
      "com.beachape"   %% "enumeratum-circe" % Version.enumeratum,
      "org.scalameta"  %% "munit"            % Version.munit % Test
    )
  )
  .dependsOn(langCore, utils)
