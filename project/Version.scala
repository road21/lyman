object Version {
  val circe      = "0.14.3"
  val scalaLib   = "3.2.2"
  val catsParse  = "0.3.9"
  val kittens    = "3.0.0"
  val enumeratum = "1.7.2"
  val munit      = "1.0.0-M7"
}
