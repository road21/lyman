package lyman.lang

import lyman.lang.core.RedYellowGreen

import scala.math.BigDecimal.RoundingMode

object prelude extends boolean, control, decimal, ryg

trait boolean:
  def not(arg: Boolean): Boolean = !arg

  extension (arg: Boolean)
    def and(other: => Boolean): Boolean = arg && other
    def or(other: => Boolean): Boolean  = arg || other

trait decimal:
  def abs(arg: BigDecimal): BigDecimal = arg.abs

  def round(arg: BigDecimal, scale: Int): BigDecimal =
    arg.setScale(scale, RoundingMode.HALF_UP)

  def decimal(str: String): BigDecimal =
    BigDecimal(str)

trait ryg:
  extension (r: RedYellowGreen)
    def req: RedYellowGreen = r

trait control extends iifs:
  def iff[A <: Out, B <: Out, Out](cond: Boolean, `then`: => A, `else`: => B): Out =
    if (cond) `then`
    else `else`
