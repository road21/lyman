package lyman.utils

object either:
  extension [A, B, C](e: Either[Either[A, B], C])
    def assocL: Either[A, Either[B, C]] = e match {
      case Left(Left(a))  => Left(a)
      case Left(Right(b)) => Right(Left(b))
      case Right(c)       => Right(Right(c))
    }

  extension [A, B, C](e: Either[A, Either[B, C]])
    def assocR: Either[Either[A, B], C] = e match {
      case Left(a)         => Left(Left(a))
      case Right(Left(b))  => Left(Right(b))
      case Right(Right(c)) => Right(c)
    }

  extension [L, R](e: Either[L, R])
    def wideLeft[L1 >: L]: Either[L1, R] = e

    def wideRight[R1 >: R]: Either[L, R1] = e
