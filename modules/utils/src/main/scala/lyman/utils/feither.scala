package lyman.utils

import cats.{Applicative, Functor, Monad, Traverse}
import cats.syntax.applicative.*
import cats.syntax.flatMap.*
import cats.syntax.functor.*
import cats.syntax.either.*
import cats.syntax.traverse.*
import lyman.utils.either.*

object feither:
  extension [F[_], L, R](e: F[Either[L, R]])
    def orElseF[L1 >: L, R1 >: R](f: => F[Either[L1, R1]])(using F: Monad[F]): F[Either[L1, R1]] =
      e.flatMap {
        case r: Right[L, R] => r.wideLeft[L1].wideRight[R1].pure[F]
        case _              => f
      }

    def orElseIn[L1 >: L, R1 >: R](f: => Either[L1, R1])(using F: Functor[F]): F[Either[L1, R1]] =
      e.map {
        case r: Right[L, R] => r.wideLeft[L1].wideRight[R1]
        case _              => f
      }

    def getOrElseF[R1 >: R](f: => F[R1])(using F: Monad[F]): F[R1] =
      e.flatMap(_.fold(_ => f, F.pure(_: R1)))

    def catchAll[R1 >: R](f: L => F[R1])(using F: Monad[F]): F[R1] =
      e.flatMap(_.fold(f, F.pure(_: R1)))

    def assocR[A, B](using F: Functor[F], ev: R <:< Either[A, B]): F[Either[Either[L, A], B]] =
      e.map {
        case Right(r) =>
          ev(r) match {
            case Left(a)      => Left(Right(a))
            case b @ Right(_) => b.leftCast
          }
        case left @ Left(_) => left.rightCast.asLeft
      }

    def assocL[A, B](using F: Functor[F], ev: L <:< Either[A, B]): F[Either[A, Either[B, R]]] =
      e.map {
        case r @ Right(_) => r.leftCast.asRight
        case Left(l) =>
          ev(l) match {
            case left @ Left(_) => left.rightCast
            case Right(b)       => b.asLeft.asRight
          }
      }

    def mapF[B](f: R => F[B])(using F: Monad[F]): F[Either[L, B]] =
      e.flatMap(_.traverse(f))

    def tapF[B](f: R => F[B])(using F: Monad[F]): F[Either[L, R]] =
      e.flatTap(_.traverse(f))

    def mapIn[B](f: R => B)(using F: Functor[F]): F[Either[L, B]] =
      e.map(_.map(f))

    def leftMapF[L1](f: L => F[L1])(using F: Monad[F]): F[Either[L1, R]] =
      e.flatMap {
        case Left(left)       => f(left).map(_.asLeft)
        case right @ Right(_) => right.leftCast[L1].pure[F]
      }

    def leftTapF[B](f: L => F[B])(using F: Monad[F]): F[Either[L, R]] =
      e.flatTap(_.swap.traverse(f))

    def leftMapIn[B](f: L => B)(using F: Functor[F]): F[Either[B, R]] =
      e.map(_.left.map(f))

    def flatMapIn[L1 >: L, B](f: R => Either[L1, B])(using F: Functor[F]): F[Either[L1, B]] =
      e.map(_.flatMap(f))

    def leftFlatMapIn[L1, R1 >: R](f: L => Either[L1, R1])(using F: Functor[F]): F[Either[L1, R1]] =
      e.map(_.leftFlatMap(f))

    def leftFlatMapF[R1 >: R, L1](f: L => F[Either[L1, R1]])(using F: Monad[F]): F[Either[L1, R1]] =
      e.flatMap {
        case Left(left)       => f(left)
        case right @ Right(_) => right.leftCast[L1].wideRight[R1].pure[F]
      }

    def doubleFlatMap[L1 >: L, R1](f: R => F[Either[L1, R1]])(using F: Monad[F]): F[Either[L1, R1]] =
      e.flatMap(_.wideLeft[L1].flatTraverse(f))

    def swapF(using F: Functor[F]): F[Either[R, L]] =
      F.map(e)(_.swap)

    def ensure[L1 >: L](f: R => Boolean, err: => L1)(using F: Functor[F]): F[Either[L1, R]] =
      flatMapIn(right => Either.cond(f(right), right, err))

    def ensureF[L1 >: L](f: R => F[Boolean], err: => F[L1])(using F: Monad[F]): F[Either[L1, R]] =
      doubleFlatMap(right => f(right).flatMap(p => Either.condF(p, right.pure[F], err)))

    def traverseF[G[_]: Applicative, R1](f: R => G[R1])(using F: Functor[F]): F[G[Either[L, R1]]] =
      e.map(_.traverse(f))

    def traverseAll[G[_]: Applicative, R1](f: R => G[R1])(using F: Traverse[F]): G[F[Either[L, R1]]] =
      F.traverse(e)(_.traverse(f))

    def leftTraverseF[G[_]: Applicative, L1](f: L => G[L1])(using F: Functor[F]): F[G[Either[L1, R]]] =
      e.map {
        case Left(left)   => f(left).map(_.asLeft)
        case r @ Right(_) => r.leftCast[L1].pure[G]
      }

    def leftTraverseAll[G[_]: Applicative, L1](f: L => G[L1])(using F: Traverse[F]): G[F[Either[L1, R]]] =
      e.traverse {
        case Left(left)   => f(left).map(_.asLeft)
        case r @ Right(_) => r.leftCast[L1].pure[G]
      }

    def productF[L1 >: L, R1](eb: => F[Either[L1, R1]])(using F: Monad[F]): F[Either[L1, (R, R1)]] =
      e.flatMap(_.wideLeft[L1].flatTraverse(r => eb.map(_.tupleLeft(r))))

    def productRF[L1 >: L, R1](eb: => F[Either[L1, R1]])(using F: Monad[F]): F[Either[L1, R1]] =
      productF(eb).map(_.map(_._2))

    def productLF[L1 >: L, R1](eb: => F[Either[L1, R1]])(using F: Monad[F]): F[Either[L1, R]] =
      productF(eb).map(_.map(_._1))

    def apF[L1 >: L, R1, Z](
                             eb: => F[Either[L1, R1]]
                           )(using F: Monad[F], ev: R <:< (R1 => Z)): F[Either[L1, Z]] =
      e.flatMap(_.wideLeft[L1].flatTraverse(r => eb.map(_.map(r))))

    def map2F[L1 >: L, R1, Z](
                               eb: => F[Either[L1, R1]]
                             )(f: (R, R1) => Z)(using F: Monad[F]): F[Either[L1, Z]] =
      productF(eb).map(_.map(f.tupled))

    def flatMap2F[L1 >: L, R1, Z](
                                   eb: => F[Either[L1, R1]]
                                 )(f: (R, R1) => F[Z])(using F: Monad[F]): F[Either[L1, Z]] =
      productF(eb).flatMap(_.traverse(f.tupled))

    def mergeF[A >: R](using ev: L <:< A, F: Functor[F]): F[A] =
      e.map(_.fold(ev, identity(_: A)))

  extension [A](id: A)
    def asRightF[F[_]: Applicative, L]: F[Either[L, A]] = id.asRight[L].pure[F]

    def asLeftF[F[_]: Applicative, R]: F[Either[A, R]] = id.asLeft[R].pure[F]

  extension [F[_], A](fa: F[A])
    def rightIn[L](using F: Functor[F]): F[Either[L, A]] = fa.map(_.asRight[L])

    def leftIn[R](using F: Functor[F]): F[Either[A, R]] = fa.map(_.asLeft[R])

  extension (o: Either.type)
    def condF[F[_], L, R](test: Boolean, r: => F[R], l: => F[L])(using F: Functor[F]): F[Either[L, R]] =
      if (test)
        r.map(_.asRight[L])
      else
        l.map(_.asLeft[R])

