package lyman

package object data {
  type StringBased[T]  = PrimBased[String, T]
}
