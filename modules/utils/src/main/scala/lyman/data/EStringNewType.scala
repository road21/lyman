package lyman.data

import io.circe.{Decoder, KeyDecoder}

abstract class EStringNewType extends StringNewtypeBase {
  def apply(s: String): Either[String, T]

  implicit val decoder: Decoder[T]       = Decoder[String].emap(apply)
  implicit val keyDecoder: KeyDecoder[T] = KeyDecoder.instance(apply(_).toOption)

  implicit val stringBased: StringBased[T] = PrimBased.create(apply, identity)
}