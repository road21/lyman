package lyman.data

trait Pattern[A] {
  def format: String
}

object Pattern {
  def instance[A](pattern: => String): Pattern[A] = new Pattern[A] {
    override val format: String = pattern
  }
}

class PatternStringNewtype(pattern: String, specName: String) extends EStringNewType {
  def apply(s: String): Either[String, T] =
    if (s.isEmpty) Left("expected non empty string")
    else if (!s.matches(pattern)) Left(s"expected string for pattern $pattern")
    else Right(s.asInstanceOf[T])

  implicit val patternInst: Pattern[T] = Pattern.instance(pattern)
}