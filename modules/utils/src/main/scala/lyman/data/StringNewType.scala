package lyman.data

import io.circe.{Decoder, KeyDecoder}

class StringNewtype extends StringNewtypeBase {
  def apply(s: String): T = s.asInstanceOf[T]

  implicit val decoder: Decoder[T]       = Decoder[String].map(apply)
  implicit val keyDecoder: KeyDecoder[T] = KeyDecoder[String].map(apply)

  implicit val stringBased: StringBased[T] = PrimBased.create(s => Right(apply(s)), identity)
}