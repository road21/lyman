package lyman.data

trait PrimBased[P, A] {
  def to(x: P): Either[String, A]
  def from(a: A): P
}
object PrimBased      {
  def apply[P, A](implicit primBased: PrimBased[P, A]): PrimBased[P, A] = primBased

  def create[P, A](f: P => Either[String, A], g: A => P): PrimBased[P, A] = new PrimBased[P, A] {
    override def to(x: P): Either[String, A] = f(x)
    override def from(a: A): P               = g(a)
  }
}