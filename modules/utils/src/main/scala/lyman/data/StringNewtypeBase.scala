package lyman.data

import cats.{Show, Order}
import io.circe.{Encoder, KeyEncoder}

class StringNewtypeBase {
  type T <: String

  implicit val show: Show[T]             = s => s
  implicit val encoder: Encoder[T]       = Encoder[String].contramap(s => s)
  implicit val keyEncoder: KeyEncoder[T] = KeyEncoder[String].contramap(s => s)
  implicit val order: Order[T]           = Order.by[T, String](identity)
}