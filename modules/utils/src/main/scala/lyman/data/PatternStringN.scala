package lyman.data

import cats.syntax.show.*

class PatternStringN(pattern: String, specName: String, val maxLength: Int) extends PatternStringNewtype(pattern, specName) {
  override def apply(s: String): Either[String, T] = {
    val trimmed = s.trim
    Either
      .cond(
        trimmed.nonEmpty && (trimmed.length <= maxLength),
        trimmed.asInstanceOf[T],
        s"string length ${trimmed.length.show}, should be in (0, ${maxLength.show}]"
      )
      .flatMap(super.apply)
  }
}