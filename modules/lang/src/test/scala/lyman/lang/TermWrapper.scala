package lyman.lang

import lyman.lang.Term.*
import lyman.lang.core.{AnyCalc, CalcType, RedYellowGreen}
import lyman.lang.data.Identifier.*
import lyman.lang.data.RefId
import lyman.lang.idents.*

import java.util.UUID
import scala.language.dynamics

case class TermWrapper(term: Term) extends AnyVal, Dynamic:
  def apply(arg: Term*): TermWrapper = TermWrapper(Apply(term, arg.toVector))

  def selectDynamic(name: String): TermWrapper =
    TermWrapper(Select(term, name.mkIdent))

  def applyDynamic(name: String)(arg: TermWrapper | Unit = ()): TermWrapper =
    arg match {
      case ()             => TermWrapper(Apply(Select(term, name.mkIdent), Vector()))
      case x: TermWrapper => TermWrapper(InfixOp(term, Ident(name.mkIdent), x.term))
    }

object TermWrapper:
  import Term.{t as tt, calc as calct, ident as identt}
  given Conversion[TermWrapper, Term] = _.term
  given Conversion[Term, TermWrapper] = TermWrapper.apply

  extension (n: BigDecimal) def t: TermWrapper = n.tt
  extension (i: Int) def t: TermWrapper        = i.tt
  extension (l: Long) def t: TermWrapper       = l.tt

  extension (b: Boolean) def t: TermWrapper = b.tt

  extension (s: String)
    def t: TermWrapper                  = s.tt
    def ident: TermWrapper              = s.identt
    def calc(tp: CalcType): TermWrapper = s.asRefId.calct(tp)

  extension (f: Float) def t: TermWrapper = f.tt

  extension (d: Double) def t: TermWrapper = d.tt

  extension (ryg: RedYellowGreen) def t: TermWrapper = ryg.tt
