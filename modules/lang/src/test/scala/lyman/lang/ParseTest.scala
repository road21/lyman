package lyman.lang

import TermWrapper.*
import lyman.lang.data.Identifier

class ParseTest extends munit.FunSuite:
  val simpleTestCases = Vector(
    "1"                    -> 1.t,
    "0.1"                  -> 0.1.t,
    "-0.1"                 -> (-0.1).t,
    "1E-9"                 -> 1e-9.t,
    "-2E-9"                -> (-2e-9).t,
    "42"                   -> 42.t,
    "lol"                  -> "lol".ident,
    "lol_kek"              -> "lol_kek".ident,
    "foo"                  -> "foo".ident,
    "\"str\""              -> "str".t,
    "\"\""                 -> "".t,
    "\"\\\"\""             -> "\"".t,
    "\"!@# $\\\"%^&*()|\"" -> "!@# $\"%^&*()|".t,
    "true"                 -> true.t,
    "false"                -> false.t,
  )

  val selectTestCases = Vector(
    "lol.kek"       -> "lol".ident.kek,
    "228.aboba"     -> 228.t.aboba,
    "a.b.c"         -> "a".ident.b.c,
    "a.b.c.d.e.f.g" -> "a".ident.b.c.d.e.f.g,
  )

  val applyTestCases = Vector(
    "func()"           -> "func".ident(),
    "func(a)"          -> "func".ident("a".ident),
    "func(a, b, c, d)" -> "func".ident("a".ident, "b".ident, "c".ident, "d".ident),
    "func()()()"       -> "func".ident()()(),
  )

  val infixOpTestCases = Vector[(String, TermWrapper)](
    "a + b"                    -> ("a".ident + "b".ident),
    "a func b"                 -> ("a".ident func "b".ident),
    "a aboba b"                -> ("a".ident aboba "b".ident),
    "a + b + c"                -> ("a".ident + "b".ident + "c".ident),
    "a + b * c+d / e"          -> ("a".ident + "b".ident * "c".ident + "d".ident / "e".ident),
    "a + b > c and x < 0"      -> (("a".ident + "b".ident > "c".ident) and ("x".ident < 0.t)),
    "rate > 0.95 and rate < 1" -> (("rate".ident > 0.95.t) and ("rate".ident < 1.t)),
    "x !@#%^&*| y"             -> ("x".ident !@#%^&*| "y".ident),
    "x+y"                      -> ("x".ident + "y".ident),
  )

  val spacingTestCases = Vector(
    "        a        " -> "a".ident,
    "   a   +    b    " -> ("a".ident + "b".ident),
    "func(a,b,c)"       -> "func".ident("a".ident, "b".ident, "c".ident),
    "func(   a  ,  b )" -> "func".ident("a".ident, "b".ident),
    "func        (arg)" -> "func".ident("arg".ident),
    "object   .  field" -> "object".ident.field
  )

  val bracketsTestCases = Vector(
    "(1 + 2) / 100"                   -> (1.t + 2.t) / 100.t,
    "((I(like(lisp))))"               -> "I".ident("like".ident("lisp".ident)),
    "   (     1    +   2    )     "   -> (1.t + 2.t),
    "1 + (2 + 3) + 4"                 -> (1.t + (2.t + 3.t) + 4.t),
    "(\"a\" * b(1 + (2 + 3) + 4, 1))" -> "a".t * "b".ident(1.t + (2.t + 3.t) + 4.t, 1.t),
  )

  val complexTestCases = Vector(
    "a.b()"      -> "a".ident.b(),
    "a().b"      -> "a".ident().b,
    "a.b().c(d)" -> "a".ident.b().c.apply("d".ident),
    "func(arg1.a, arg2.a, arg3.a(), arg4())" ->
      "func".ident("arg1".ident.a, "arg2".ident.a, "arg3".ident.a(), "arg4".ident()),
    "a(b(c(d(e.e.e))))"  -> "a".ident("b".ident("c".ident("d".ident("e".ident.e.e)))),
    "func(a + b, c - d)" -> "func".ident("a".ident + "b".ident, "c".ident - "d".ident),
    "iif(rate == 1, green, rate >= 0.95 and rate < 1, yellow, red)" -> "iif".ident(
      "rate".ident.term.infix(Identifier("=="), 1.t),
      "green".ident,
      ("rate".ident >= 0.95.t) and ("rate".ident < 1.t),
      "yellow".ident,
      "red".ident
    )
  )

  def parseTest(expressionType: String, testCases: Vector[(String, TermWrapper)]): Unit =
    test(s"parse $expressionType expression") {
      testCases.foreach(tc =>
        val parsed = parse(tc._1)
        assertEquals(parsed.map(_._2), Right(tc._2.term), parsed)
      )
    }

  parseTest("simple", simpleTestCases)
  parseTest("select", selectTestCases)
  parseTest("apply", applyTestCases)
  parseTest("spacing", spacingTestCases)
  parseTest("infixOp", infixOpTestCases)
  parseTest("brackets", bracketsTestCases)
  parseTest("complex", complexTestCases)
