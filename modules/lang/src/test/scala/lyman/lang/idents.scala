package lyman.lang

import lyman.lang.core.CalcType
import lyman.lang.data.{RefId, RefName, RefNs}

object idents:
  extension (R: RefId.type) def mk(ns: RefNs, name: RefName): RefId = s"$ns.$name".asRefId

  extension (str: String)
    def asRefId: RefId =
      RefId(str).fold(
        err => throw new Exception(s"illegal calculation id $str, $err"),
        identity
      )

    def asRefName: RefName =
      RefName(str).fold(
        err => throw new Exception(s"illegal calculation name $str, $err"),
        identity
      )

    def asRefNs: RefNs =
      RefNs(str).fold(
        err => throw new Exception(s"illegal namespace $str, $err"),
        identity
      )
