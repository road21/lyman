package lyman.lang

import io.circe.syntax.*
import lyman.lang.core.{RedYellowGreen, TBool, TDecimal, TStr}
import TermWrapper.*

class CodecTest extends munit.FunSuite:
  val m1 = "ms.x"
  val m2 = "ms.y"

  val testCases = Vector[Term](
    true.t || false.t,
    (1.t + 2.3.t + 3.t + 4.t) / (2.t * 1.t) + 1000000000000L.t,
    "foo".t.boo.coo + "kek".t + "cheburek".t,
    m1.calc(TBool),
    m1.calc(TDecimal) * m1.calc(TDecimal),
    "iif".ident(
      m1.calc(TDecimal) < 0.1.t,
      RedYellowGreen.green.t,
      m1.calc(TDecimal) > 0.8.t,
      RedYellowGreen.red.t,
      RedYellowGreen.yellow.t
    )
  )

  test("encode and decode terms") {
    testCases.foreach(tc => assertEquals[Any, Any](tc.asJson.as[Term], Right(tc)))
  }
