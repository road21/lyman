package lyman.lang

import Term.bd
import TermWrapper.*
import lyman.lang.core.{AnyCalc, Arguments, CalcType, RedYellowGreen, TBool, TDecimal, TStr}
import lyman.lang.data.RefId
import lyman.lang.errors.CompileAndRunError
import lyman.lang.idents.*
import munit.FunSuite

import java.util.UUID

case class RunLinkedCase[A](term: Term, typ: CalcType, args: Map[RefId, AnyCalc], result: A)

object RunLinkedCase:
  def apply[A: IsSinRepr](term: Term, args: Map[String, AnyCalc] = Map.empty, result: A): RunLinkedCase[A] =
    RunLinkedCase(term, summon[IsSinRepr[A]].value, args.map { case (x, y) => x.asRefId -> y }, result)

class RunTest extends munit.FunSuite:
  val m1 = "ms.x"
  val m2 = "ms.y"

  val testCases = Vector(
    RunLinkedCase(true.t || false.t, result = true),
    RunLinkedCase(
      (1.t + 2.t + 3.t + 4.t) / (2.t * 1.t) + 1000000000000L.t,
      result = BigDecimal(1000000000005L)
    ),
    RunLinkedCase("aboba".t + "kek".t, result = "aboba" + "kek"),
    RunLinkedCase(2.28.t / 3.37.t, result = 2.28.bd / 3.37.bd),
    RunLinkedCase(m1.calc(TBool), Map(m1 -> true), result = true),
    RunLinkedCase(m1.calc(TStr), Map(m1 -> "lol"), result = "lol"),
    RunLinkedCase(m1.calc(TDecimal) * m1.calc(TDecimal), Map(m1 -> 2.bd), result = 4.bd),
    RunLinkedCase(m1.calc(TDecimal) * m2.calc(TDecimal), Map(m1 -> 4.bd, m2 -> 8.bd), result = 32.bd),
    RunLinkedCase(
      "iif".ident(m1.calc(TBool), RedYellowGreen.green.t, RedYellowGreen.red.t),
      Map(m1 -> true),
      result = RedYellowGreen.green
    )
  )

  test("eval simple linked expression") {
    testCases.foreach { tc =>
      val res = api.compileAndRun(tc.term, tc.typ, tc.args)
      assertEquals((res: Either[CompileAndRunError, Any]), Right(tc.result), res)
    }
  }
