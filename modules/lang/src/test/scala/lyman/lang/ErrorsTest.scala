package lyman.lang

import lyman.lang.Term.bd
import lyman.lang.TermWrapper.*
import lyman.lang.core.*
import lyman.lang.data.RefId
import lyman.lang.errors.{CompileAndRunError, CompileError}
import lyman.lang.errors.Compile.MsgKind
import lyman.lang.idents.*
import munit.FunSuite

import java.util.UUID

class ErrorsTest extends munit.FunSuite:
  val m1 = "ms.x"

  def isRuntime(f: PartialFunction[Throwable, Boolean]): CompileAndRunError => Boolean =
    case errors.Runtime(x) => f.isDefinedAt(x) && f(x)
    case _                 => false

  def isCompile(kind: MsgKind): CompileAndRunError => Boolean =
    case errors.Compile(x) => x.head.kind == kind && x.tail.isEmpty
    case _                 => false

  val testCases = Vector[(Term, CalcType, Map[String, AnyCalc], CompileAndRunError => Boolean)](
    (m1.calc(TDecimal) / 0.t, TDecimal, Map(m1 -> 100.bd), isRuntime { case _: ArithmeticException => true }),
    (true.t, TStr, Map(), isCompile(MsgKind.TypeMismatch)),
    ("cs.foo".ident, TStr, Map(), isCompile(MsgKind.NotFound)),
    (
      "iif".ident(m1.calc(TDecimal), RedYellowGreen.green.t, RedYellowGreen.red.t),
      TRYG,
      Map(),
      isCompile(MsgKind.TypeMismatch)
    ),
    ("iif".ident(m1.calc(TBool), RedYellowGreen.green.t, true.t), TRYG, Map(), isCompile(MsgKind.TypeMismatch)),
    (
      "if".ident(m1.calc(TBool), RedYellowGreen.green.t, RedYellowGreen.red.t),
      TRYG,
      Map(),
      isCompile(MsgKind.NotFound)
    )
  )

  test("errors in simple expression") {
    testCases.foreach { case (term, typ, args, prop) =>
      val res = api.compileAndRun(term, typ, args.map((k, v) => (k.asRefId, v)))
      assertEquals(res.left.map(prop), Left(true), res)
    }
  }
