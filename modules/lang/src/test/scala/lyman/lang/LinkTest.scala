package lyman.lang

import TermWrapper.*
import cats.Id
import lyman.lang.idents.*
import lyman.lang.core.{AnyCalc, Arguments, CalcType, RedYellowGreen, TBool, TDecimal, TRYG, TStr}
import lyman.lang.data.{RefName, RefNs}
import lyman.lang.linker.CalcTyped

class LinkTest extends munit.FunSuite:
  val refs = Map(
    (Some("ms"), "x")       -> ("ms.x", TBool),
    (None, "y")             -> ("ms.y", TBool),
    (Some("ms2"), "x")      -> ("ms2.x", TDecimal),
    (Some("ms2"), "y")      -> ("ms2.y", TDecimal),
    (Some("ms2"), "y.req")  -> ("ms2.y.req", TRYG),
    (Some("ms"), "boo.foo") -> ("ms.boo.foo", TRYG),
    (Some("ms3"), "r")      -> ("ms3.r", TRYG),
  ).map { case ((ms, mn), (mId, mTy)) =>
    (ms.map(_.asRefNs), mn.asRefName) -> CalcTyped(mId.asRefId, mTy)
  }

  given Refer[Id] = (set: Option[RefNs], name: RefName) => refs.get(set -> name)

  val testCases = Vector(
    "ms".ident.x -> ("ms.x".calc(TBool), Set("ms.x")),
    "ms2".ident.x * "ms2".ident.y -> ("ms2.x".calc(TDecimal) * "ms2.y".calc(TDecimal), Set(
      "ms2.x",
      "ms2.y"
    )),
    "ms3".ident.x * "ms2".ident.y -> ("ms3".ident.x * "ms2.y".calc(TDecimal), Set("ms2.y")),
    "iif".ident("y".ident, "ms2".ident.y.req, "ms".ident.boo.foo) -> (
      (
        "iif".ident("ms.y".calc(TBool), "ms2.y.req".calc(TRYG), "ms.boo.foo".calc(TRYG)),
        Set("ms.y", "ms2.y.req", "ms.boo.foo")
      )
    ),
    "iif".ident("y".ident, "ms3".ident.r.req, "ms".ident.boo.foo) -> (
      (
        "iif".ident("ms.y".calc(TBool), "ms3.r".calc(TRYG).req, "ms.boo.foo".calc(TRYG)),
        Set("ms.y", "ms3.r", "ms.boo.foo")
      )
    )
  ).map { case (from, (to, deps)) =>
    from.term -> (to.term, deps.map(_.asRefId))
  }

  test("link expressions") {
    testCases.foreach { case (from, (to, deps)) =>
      val linked = linker[Id](from)
      assertEquals(linked.term, to)
      assertEquals(linked.depends, deps)
    }
  }
