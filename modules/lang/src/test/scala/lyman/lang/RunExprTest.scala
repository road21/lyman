package lyman.lang

import Term.bd
import TermWrapper.*
import lyman.lang.core.{AnyCalc, Arguments, CalcType, TBool, TDecimal, TStr, RedYellowGreen as ryg}
import lyman.lang.data.{RefId, RefName, RefNs}
import lyman.lang.errors.{CompileAndRunError, CompileError}
import lyman.lang.idents.*
import munit.FunSuite
import cats.Id
import lyman.lang.linker.CalcTyped
import Typed.tpd

import java.time.Clock
import java.util.UUID
import scala.collection.immutable.Map

enum Ref:
  case Id(ref: RefId)
  case Name(name: RefName)

object Ref:
  def apply(x: String): Ref =
    RefId(x).fold[Ref](_ => Ref.Name(x.asRefName), Ref.Id(_))

case class Typed(value: AnyCalc, typ: CalcType)
object Typed:
  extension [A <: AnyCalc](v: A) def tpd(using IsSinRepr[A]): Typed = Typed(v, IsSinRepr[A].value)

case class ExpectRes[A](refs: Map[Ref, Typed], result: A):
  def results(defaultNs: RefNs): Map[RefId, AnyCalc] =
    refs.map {
      case (Ref.Id(r), t)   => r                      -> t.value
      case (Ref.Name(r), t) => RefId.mk(defaultNs, r) -> t.value
    }

  def mkRefer(defaultNs: RefNs): Refer[Id] = (set: Option[RefNs], name: RefName) =>
    set
      .fold(
        refs.get(Ref.Name(name)).map(RefId.mk(defaultNs, name) -> _)
      ) { ns =>
        val refId = RefId.mk(ns, name)
        refs.get(Ref.Id(refId)).map(refId -> _)
      }
      .map { case (id, t) => CalcTyped(id, t.typ) }

case class RunExprCase(expr: String, typ: CalcType, results: Vector[ExpectRes[AnyCalc]])

object RunExprCase:
  def apply[A <: AnyCalc](expr: String, args: Vector[(Map[String, Typed], A)])(using IsSinRepr[A]): RunExprCase =
    RunExprCase(
      expr,
      IsSinRepr[A].value,
      args.map { case (m, r) =>
        ExpectRes(m.map { case (x, t) => Ref(x) -> t }, r)
      }
    )

class RunExprTest extends munit.FunSuite:
  val testCases = Vector(
    {
      val rate = "rej_res_rate"
      RunExprCase(
        s"iif($rate == 1, green, $rate >= 0.95 and $rate < 1, yellow, red)",
        Vector(
          Map(rate -> 0.0001.bd.tpd)  -> ryg.red,
          Map(rate -> 0.94999.bd.tpd) -> ryg.red,
          Map(rate -> 0.95.bd.tpd)    -> ryg.yellow,
          Map(rate -> 0.9501.bd.tpd)  -> ryg.yellow,
          Map(rate -> 0.999.bd.tpd)   -> ryg.yellow,
          Map(rate -> 1.bd.tpd)       -> ryg.green,
          Map(rate -> 1.001.bd.tpd)   -> ryg.red,
          Map(rate -> 2.bd.tpd)       -> ryg.red
        )
      )
    }, {
      val real     = "application_product_scoring.real_increase"
      val forecast = "application_product_scoring.forecast_increase"
      val abs      = s"(abs($real-$forecast)/$forecast)"
      RunExprCase(
        s"""iif(0.85 <= $abs and $abs <= 1.15, green, 0.8 <= $abs and $abs <= 1.2, yellow, red)""",
        Vector(
          Map(real -> 0.1.bd.tpd, forecast -> 0.1.bd.tpd)           -> ryg.red,
          Map(real -> 0.2.bd.tpd, forecast -> 0.1.bd.tpd)           -> ryg.green,
          Map(real -> 0.bd.tpd, forecast -> 0.2.bd.tpd)             -> ryg.green,
          Map(real -> 2.2.bd.tpd, forecast -> 1.0.bd.tpd)           -> ryg.yellow,
          Map(real -> 0.592146.bd.tpd, forecast -> 0.32897.bd.tpd)  -> ryg.yellow,
          Map(real -> 0.5888563.bd.tpd, forecast -> 0.32897.bd.tpd) -> ryg.red
        )
      )
    }, {
      val (m1, m2, m3) = ("m1", "m2", "m3")
      RunExprCase(
        s"""($m1 * 0.2 + $m2 * 0.3 + $m3 * 0.5)/3""",
        Vector(
          Map(
            m1 -> 0.005050895016460946.bd.tpd,
            m2 -> 0.015161345669536697.bd.tpd,
            m3 -> -4.270467552477447.bd.tpd
          ) -> BigDecimal("-0.7098917311781901005666666666666667")
        )
      )
    }, {
      val (m1, m1r, m2, m3) = ("m1", "m1.req", "m2", "m3")
      RunExprCase(
        s"""iif($m1r == $m3.req, $m1, $m2 > 0, 100, 0)""",
        Vector(
          Map(
            m1  -> 50.bd.tpd,
            m1r -> ryg.red.tpd,
            m2  -> 10.bd.tpd,
            m3  -> ryg.yellow.tpd,
          ) -> 100.bd,
          Map(
            m1  -> 50.bd.tpd,
            m1r -> ryg.green.tpd,
            m2  -> 10.bd.tpd,
            m3  -> ryg.green.tpd,
          ) -> 50.bd
        )
      )
    }
  )

  test("eval simple expression") {
    testCases.foreach { tc =>
      api.parse(tc.expr).map { ast =>
        tc.results.foreach { r =>
          val defaultNs   = "default".asRefNs
          given Refer[Id] = r.mkRefer(defaultNs)
          val linked      = api.link[Id](ast).term

          val res = api.compileAndRun(linked, tc.typ, r.results(defaultNs))
          assertEquals((res: Either[CompileAndRunError, Any]), Right(r.result), res)
        }
      }
    }
  }

  test("compile and then run separately") {
    testCases.foreach { tc =>
      api.parse(tc.expr).map { ast =>
        tc.results.foreach { r =>
          val defaultNs   = "default".asRefNs
          given Refer[Id] = r.mkRefer(defaultNs)
          val linked      = api.link[Id](ast).term

          val byteCode: Either[CompileError, ByteCode] = api.compile(linked, tc.typ)
          val res: Either[CompileAndRunError, Any] =
            byteCode.flatMap(code => api.run(code, tc.typ, r.results(defaultNs)))
          assertEquals((res: Either[CompileAndRunError, Any]), Right(r.result), res)
        }
      }
    }
  }
