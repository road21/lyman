package lyman.lang

import dotty.tools.dotc.ast.Trees.{DocComment, Tree, Untyped}
import dotty.tools.dotc.ast.untpd
import dotty.tools.dotc.core.Names
import dotty.tools.dotc.core.Constants.Constant as DConstant
import dotty.tools.dotc.util.{NoSource, SourceFile, Spans}
import dotty.tools.dotc.ast.untpd.{
  AppliedTypeTree,
  Apply,
  DefDef,
  Ident,
  InfixOp,
  Literal,
  Number,
  NumberKind,
  PackageDef,
  Select,
  TypeApply,
  TypeTree
}
import dotty.tools.dotc.core.Contexts.Context
import lyman.lang.Term
import lyman.lang.core.{CalcType, TBool, TDecimal, TRYG, TStr}

object Translator:
  val dummySpan = Spans.Span(0)

  def apply(term: Term)(using SourceFile): Tree[Untyped] =
    translate(term)

  // TODO: make it stack safe
  def translate(term: Term)(using SourceFile): Tree[Untyped] =
    term match {
      case Term.Literal(c) => constant(c)
      case Term.Ident(n)   => Ident(Names.termName(n))
      case Term.InfixOp(l, i, r) =>
        InfixOp(translate(l), ident(i), translate(r))
      case Term.Apply(l, r) =>
        Apply(translate(l), r.map(translate).toList)
      case Term.Select(q, n) =>
        Select(translate(q), Names.termName(n))
      case Term.Calculation(u, t) =>
        Apply(
          TypeApply(Ident("calc".mkTermName), List(translateType(t))),
          List(constant(Constant.PString(u)))
        )
    }

  def translateType(t: CalcType)(using SourceFile): untpd.Tree = {
    val typeName = t match
      case TDecimal => "BigDecimal"
      case TBool    => "Boolean"
      case TStr     => "String"
      case TRYG     => "RedYellowGreen"

    Ident(typeName.mkTypeName)
  }

  def ident(ident: Term.Ident)(using SourceFile): Ident =
    Ident(Names.termName(ident.name))

  def constant(cnst: Constant)(using SourceFile): Tree[Untyped] =
    cnst match {
      case Constant.PBoolean(b) => Literal(DConstant(b))
      case Constant.PString(s)  => Literal(DConstant(s))
      case Constant.PDecimal(d) => Apply(Ident("BigDecimal".mkTermName), List(Literal(DConstant(d.toDouble))))
    }
