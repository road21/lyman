package lyman.lang

import enumeratum.{CirceEnum, Enum, EnumEntry}
import lyman.lang.core.{TBool, TDecimal, TRYG, TStr, CalcType}
import io.circe.{Decoder, Encoder}

object codecs extends CalcTypeCodec

trait CalcTypeCodec:
  given Encoder[CalcType] = Encoder[CalcTypeRepr].contramap(CalcTypeRepr.fromCalcType)
  given Decoder[CalcType] = Decoder[CalcTypeRepr].map(_.toCalcType)

sealed trait CalcTypeRepr extends EnumEntry:
  def toCalcType: CalcType = this match
    case CalcTypeRepr.bool    => TBool
    case CalcTypeRepr.str     => TStr
    case CalcTypeRepr.decimal => TDecimal
    case CalcTypeRepr.ryg     => TRYG

object CalcTypeRepr extends Enum[CalcTypeRepr], CirceEnum[CalcTypeRepr]:
  case object bool    extends CalcTypeRepr
  case object str     extends CalcTypeRepr
  case object decimal extends CalcTypeRepr
  case object ryg     extends CalcTypeRepr

  def values = findValues

  def fromCalcType(mt: CalcType): CalcTypeRepr = mt match
    case TBool    => bool
    case TStr     => str
    case TDecimal => decimal
    case TRYG     => ryg
