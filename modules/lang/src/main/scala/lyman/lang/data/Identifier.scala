package lyman.lang.data

import cats.Hash
import lyman.data.{PatternStringN, PatternStringNewtype, StringNewtype}

object RefId extends PatternStringNewtype(regex.calculation, "RefId"):
  given Hash[RefId] = Hash.fromUniversalHashCode

object RefNs extends PatternStringN(regex.ident, "RefNamespace", 30)

object RefName extends PatternStringNewtype(regex.refName, "RefName")

// TODO: add regex
object Identifier extends StringNewtype:
  given Hash[Identifier]                        = Hash.fromUniversalHashCode
  extension (s: String) def mkIdent: Identifier = Identifier(s)

object regex:
  val ident       = "[a-zA-Z][a-zA-Z0-9_]*"
  val id          = s"$ident\\.$ident"
  val refName     = s"$ident(\\.$ident)?"
  val calculation = s"$ident\\.$refName"
