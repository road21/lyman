package lyman.lang

package object data:
  type RefId   = RefId.T
  type RefNs   = RefNs.T
  type RefName = RefName.T

  type Identifier = Identifier.T
