package lyman.lang

case class ByteCode(classCode: Array[Byte], objectCode: Array[Byte])
