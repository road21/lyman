package lyman.lang

import linker.Result
import cats.{Applicative, Functor, Monad}
import cats.syntax.applicative.*
import cats.syntax.apply.*
import cats.syntax.applicativeError.*
import cats.syntax.functor.*
import cats.syntax.flatMap.*
import cats.syntax.traverse.*
import lyman.lang.core.CalcType
import lyman.lang.data.{Identifier, RefId, RefName, RefNs}
import lyman.utils.foption.*
import linker.CalcTyped
import Term.{Ident, InfixOp}

trait Refer[F[_]]:
  def ref(set: Option[RefNs], name: RefName): F[Option[CalcTyped]]

object Refer:
  @inline def apply[F[_]](using r: Refer[F]): Refer[F] = r

object linker:
  case class Result(term: Term, depends: Set[RefId])
  case class CalcTyped(id: RefId, typ: CalcType)

  def apply[F[_]: Monad: Refer](term: Term): F[Result] = link(term)

  private def deref[F[_]: Refer: Monad](nsName: Option[RefNs], name: RefName): F[Option[Result]] =
    Refer[F]
      .ref(nsName, name)
      .mapIn(t => Result(Term.Calculation(t.id, t.typ), Set(t.id)))

  private def link[F[_]: Monad: Refer](term: Term): F[Result] =
    term match {
      case Ident(name) =>
        RefName(name).toOption
          .flatTraverse(mName => deref[F](None, mName))
          .map(_.getOrElse(Result(term, Set())))

      case Term.Select(select @ Term.Select(Ident(nsName), mName), rName) =>
        (RefNs(nsName).toOption, RefName(s"$mName.$rName").toOption)
          .traverseN[F, Option[Result]] { case (sName, mName) =>
            deref[F](Some(sName), mName)
          }
          .map(_.flatten)
          .getOrElseF(
            link(select).map(r => Result(Term.Select(r.term, rName), r.depends))
          )

      case Term.Select(ident @ Ident(fstName), sndName) =>
        val tryWithNs = (RefNs(fstName).toOption, RefName(sndName).toOption)
          .traverseN[F, Option[Result]] { case (sName, mName) =>
            deref[F](Some(sName), mName)
          }
          .map(_.flatten)

        RefName(s"$fstName.$sndName").toOption
          .flatTraverse(name => deref[F](None, name))
          .orElseF(tryWithNs)
          .getOrElseF(
            link(ident).map(r => Result(Term.Select(r.term, sndName), r.depends))
          )

      case Term.Select(term, method) =>
        link(term).map(r => Result(Term.Select(r.term, method), r.depends))

      case Term.Apply(term, args) =>
        (link(term), args.traverse(link)).mapN { case (tres, argsRes) =>
          val (tArgs, deps) = argsRes.foldLeft((Vector[Term](), Set[RefId]())) {
            case ((args, ids), Result(arg, depends)) =>
              (args :+ arg, ids ++ depends)
          }
          Result(Term.Apply(tres.term, tArgs), tres.depends ++ deps)
        }
      case Term.InfixOp(left, op, right) =>
        (link(left), link(right)).mapN { case (Result(lTerm, lDeps), Result(rTerm, rDeps)) =>
          Result(InfixOp(lTerm, op, rTerm), lDeps ++ rDeps)
        }
      case x => Result(x, Set()).pure[F]
    }
